
const FIRST_NAME = "ALINA";
const LAST_NAME = "BOARCA";
const GRUPA = "1075";


class Employee {
    constructor(name,surname,salary)
    {
    	this.name=name;
    	this.surname=surname;
    	this.salary=salary;

    }

    getDetails()
    {
    	return this.name+" "+this.surname+" "+this.salary;
    }
}

class SoftwareEngineer extends Employee {
   constructor(name, surname, salary, experience/*='JUNIOR'*/){
	   super(name, surname, salary);
	   if(experience){
		   this.experience=experience;
	   }
	   else {
		   this.experience='JUNIOR'
	   }
   }
   
   applyBonus(){
	   if(this.experience=='MIDDLE')
		   return this.salary*1.15;
	   else if(this.experience=='SENIOR')
		   return this.salary*1.2;
		   
		   else return this.salary*1.1;
   }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

